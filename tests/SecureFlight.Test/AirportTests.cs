using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Infrastructure;
using SecureFlight.Infrastructure.Repositories;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public async Task Update_Succeeds()
        {
            using (var context = GetContext())
            {
                var repository = new BaseRepository<Airport>(context);
                var enitityToUpade = new Airport
                {
                    Code = "AAQ",
                    Name = "Anapa Vityazevo",
                    City = "Porto Alegre",
                    Country = "Brazil"
                };

                repository.Update(enitityToUpade);

                var airports = await repository.GetAllAsync();
                Assert.True(airports.Count == 4);
                var updatedAirport = airports.FirstOrDefault(a => a.Code.Equals("AAQ"));
                Assert.NotNull(updatedAirport);
                Assert.Equal("Porto Alegre", updatedAirport.City);
                Assert.Equal("Brazil", updatedAirport.Country);
            }
        }

        private SecureFlightDbContext GetContext()
        {
            var options = new DbContextOptionsBuilder<SecureFlightDbContext>().UseInMemoryDatabase("SecureFlightTest").Options;

            using (var context = new SecureFlightDbContext(options))
            {
                context.Database.EnsureCreated();
            }

            return new SecureFlightDbContext(options);
        }
    }
}
