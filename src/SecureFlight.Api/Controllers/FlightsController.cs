﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightsController : ControllerBase
    {
        private readonly IService<Flight> _flightService;
        private readonly IRepository<PassengerFlight> _flightPasengerRepository;
        private readonly IRepository<Passenger> _pasengerRepository;

        public FlightsController(IService<Flight> flightService, IRepository<PassengerFlight> passengerFlightrepository, IRepository<Passenger> passengerRepository)
        {
            _flightService = flightService;
            _flightPasengerRepository = passengerFlightrepository;
            _pasengerRepository = passengerRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var flights = (await _flightService.GetAllAsync()).Result
                .Select(x => new FlightDataTransferObject
                {
                    Id = x.Id,
                    ArrivalDateTime = x.ArrivalDateTime,
                    Code = x.Code,
                    FlightStatusId = (int) x.FlightStatusId,
                    DepartureDateTime = x.DepartureDateTime,
                    DestinationAirport = x.DestinationAirport,
                    OriginAirport = x.OriginAirport
                });

            return Ok(flights);
        }

        [HttpPost]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public IActionResult AddFlightPassenger(long flightId, string passengerId)
        {
            _flightPasengerRepository.Create(new PassengerFlight
            {
                FlightId = flightId,
                PassengerId = passengerId
            });

            return Ok(true);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public IActionResult RemoveFlightPassenger(long flightId, string passengerId)
        {
            _flightPasengerRepository.Delete(new PassengerFlight
            {
                FlightId = flightId,
                PassengerId = passengerId
            });

            return Ok(true);
        }
    }
}
